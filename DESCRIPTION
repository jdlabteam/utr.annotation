Package: utr.annotation
Type: Package
Title: Annotate Variants in the Untranslated Regions
Version: 1.0.3
Authors@R: 
	c(person(given = "Yating",
	  	family = "Liu",
		role = c("aut", "cre"),
		email = "yatinghust@gmail.com",
		comment = c("https://orcid.org/0000-0001-7425-0422")),
	  person(given = "Joseph D.",
		family = "Dougherty",
		role = "aut",
		email = "jdougherty@wustl.edu",
		comment = "https://orcid.org/0000-0002-6385-3997"))
Maintainer: Yating Liu <yatinghust@gmail.com>
Description: A fast, user-friendly tool for annotating potential deleterious variants in the untranslated regions for both human and mouse species. Y Liu, JD Dougherty (2021) <doi:10.1101/2021.06.23.449510>.
License: GPL (>= 3)
Encoding: UTF-8
SystemRequirements: 
	Linux or MacOS Intel chip
	tensorflow (1.14.0)
Depends:
	R (>= 3.5.0)
Imports:
	parallel,
	doParallel,
	data.table,
	readr,
	stringr,
	biomaRt,
	vcfR,
	Biostrings,
	dplyr,
	tidyr,
	keras,
	AnnotationHub,
	ensembldb,
	AnnotationFilter,
	BiocGenerics,
	GenomicFeatures,
	GenomicRanges,
	IRanges,
	foreach,
	rtracklayer,
	stats,
	utils,
	xml2
Suggests: 
    testthat,
    knitr,
    rmarkdown,
    ggplot2
RoxygenNote: 7.1.1
VignetteBuilder: knitr
