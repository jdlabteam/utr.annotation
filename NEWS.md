# utr.annotation 1.0.3
* Removed deep learning model from utr.annotation package and hosted the model file in MRL.dl.model package on Bitbucket

# utr.annotation 1.0.2
* Added examples to external functions
* Added verbose parameter to suppress diagnostic message by default


